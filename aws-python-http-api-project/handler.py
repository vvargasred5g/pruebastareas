import json
from PyPDF2 import PdfFileWriter, PdfFileReader
import io
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter


def hello(event, context):
    data=json.loads(event['body'])
    i1=25
    try:
        packet = io.BytesIO() #pendiente en investigar
        can = canvas.Canvas(packet, pagesize=letter) #establece formato de la hoja

        #page 1 (part I)
        #can.drawString(65, 544, data.get('benOwner',''))            # Name of individual who is the beneficial owner 
        #can.drawString(394, 544, data.get('citCountry',''))      #Country of citizenship
        #can.drawString(65, 520, data.get('permResidAddr',''))       #Permanent residence address  
        #can.drawString(65, 496, data.get('permCitTownState',''))    #City or town, state or province
        #can.drawString(446, 496, data.get('permCountry',''))        #Country of permanent residence
        #can.drawString(65, 472, data.get('mainAddr',''))            #Mailing address 
        #can.drawString(65, 447, data.get('mainCitTownState',''))    #City or town, state or province
        #can.drawString(446, 447, data.get('mainCountry',''))        #mainling country
        #can.drawString(65, 423, data.get('taxpayNumber',''))        #U.S. taxpayer identification number 
        #can.drawString(65, 399, data.get('foreignTax',''))          #Foreign tax identifying number  
        #can.drawString(565, 411.5, data.get('checkNoFTIN',''))      #checkbox to confirm not legally required FTIN
        #can.drawString(310, 399, data.get('FTIN',''))               #FTIN
        #can.drawString(65, 374.5, data.get('refNumb',''))           #Reference number(s) 
        #can.drawString(310, 374.5, data.get('dateBirth',''))        #Date of birth (MM-DD-YYYY) 
        
        # checkbox federal tax clasification
        #pagina 1
        can.setFont('Helvetica', 8) #establece el formato de la letra
        can.drawString(90, 684, data.get('benOwner',''))  
        can.showPage()#selecciona paginas de plantilla transparente en orden de aparicion
        
        #pagina 3
        can.setFont('Helvetica', 8) #establece el formato de la letra
        can.drawString(90, 684, data.get('permResidAddr','')) 
        can.showPage()#esta selecciona la pagina 2 (la cual se identifica con el indice 1)

        can.save() #guarda el pdf transparente
        packet.seek(0) #para que empieze a leer
        

        new_pdf = PdfFileReader(packet) #guardar un nuevo pdf canvas
        existing_pdf = PdfFileReader(open("Doc1.pdf", "rb"))#direcciona y lee el pdf existente
        output = PdfFileWriter()
        page = []
        for i in range(3):
            page.append(existing_pdf.getPage(i)) #guarda las paginas del pdf eistente en una lista

        page[0].mergePage(new_pdf.getPage(0))# mezcla la pagina 1 del pdf existente con la pagina 1 de la plantilla transparente
        page[2].mergePage(new_pdf.getPage(1))# mezcla la pagina 3 del pdf existente (indice 2) con la pagina 2 de la plantilla transparente (indice 1)
        
        for i in range(3):
            output.addPage(page[i]) #se añaden los cambios que quieren mostrarse en el pdf de salida

        outputStream = open('Doc1mod.pdf', "wb")#genera el pdf de salida
        output.write(outputStream)#guarda la fusion de los archivos en el pdf de salida
        outputStream.close()#cierra el pdf creado

        res = 'operacion exitosa' #operacion exitosa
        
    except Exception as e:
        res = str(e) #devuelve la excepcion si sale mal
        print(str(e))

    return res


