import json
from db import Session
from sqlalchemy.exc import IntegrityError
from sqlalchemy.sql import func
from property_rent import PropertyRent
from property_expense import PropertyExpense
from property import Property
from state import State
from city import City
from property_price import PropertyPrice

def returnRentList(event, context):
    try:

        bd = Session()
        ext = bd.query(PropertyRent).filter(PropertyRent.status == 1)

        response_prop = []
        for row in ext:
            dict_data = {
                #"status": row.status,
                "id": row.id,
                "date": str(row.created.strftime("%Y-%m-%d")),
                "value": row.value,
                "taxes": row.taxes,
                "totExpense": row.expenses,
                "netRent": row.netrent,
                "property": row.property,
                "provisions": row.provision,
                "cash": row.cash
            }
            response_prop.append(dict_data)
        bd.close()

        codigo = 200
        message = response_prop

    except IntegrityError as E:
        bd.rollback()
        codigo = 500
        message = f"ERROR:{E}"
    except Exception as E:
        bd.rollback()
        codigo = 500
        message = f"ERROR:{E}"

    response = {
        "statusCode": codigo,
        'headers': {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
        },
        "body": json.dumps({
            "statusCode": codigo,
            "message": message
        })
    }

    return response

def returnInfo(event, context):
    data = json.loads(event['body'])
    try:
        bd = Session()

        ext = bd.query(PropertyRent)\
            .filter((PropertyRent.id == int(data['id'])) 
            & (PropertyRent.status == 1))
        ext2 = bd.query(PropertyExpense)\
            .filter((PropertyExpense.rent == int(data['id'])) 
                    & (PropertyExpense.status == 1))
        response_exp = []
        for row in ext2:
            dic = {
                #"status2": f"{row.status}",
                "expDate": f"{row.exp_date}",
                "description": f"{row.description}",
                "ExpValue": float(f"{row.value}")
            }
            response_exp.append(dic)

        response_prop = []
        for row in ext:
            dict_data = {
                "value": row.value,
                "taxes": row.taxes,
                "totExpense": row.expenses,
                "ExpDetail": response_exp,
                "netRent": row.netrent,
                "provisions": row.provision,
                "cash": row.cash
            }
            response_prop.append(dict_data)
        bd.close()
        codigo = 200
        message = response_prop
    except IntegrityError as E:
        bd.rollback()
        codigo = 500
        message = f"ERROR:{E}"
    except Exception as E:
        bd.rollback()
        codigo = 500
        message = f"ERROR:{E}"

    response = {
        "statusCode": codigo,
        'headers': {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
        },
        "body": json.dumps({
            "statusCode": codigo,
            "message": message
        })
    }
    return response

def deleteRent(event, context):
    data = json.loads(event['body'])
    try:
        bd = Session()
        selector = bd.query(PropertyRent).filter(
            PropertyRent.id == int(data['id']))
        for row in selector:
            var = int(row.status)
        bd.query(PropertyRent)\
            .filter((PropertyRent.id == int(data['id'])) 
                    & (PropertyRent.status == 1))\
            .update({PropertyRent.status: 0})
        bd.commit()
        bd.query(PropertyExpense)\
            .filter((PropertyExpense.rent == int(data['id'])) 
                    & (PropertyExpense.status == 1))\
            .update({PropertyExpense.status: 0})
        bd.commit()
        bd.close()

        codigo = 200
        if var == 1:
            message = f"Rent identified by id:{int(data['id'])} was removed sucessfully"
        elif var == 0:
            message = f"Rent identified by id:{int(data['id'])} is already removed"
    except IntegrityError as E:
        bd.rollback()
        codigo = 500
        message = f"ERROR:{E}"
    except Exception as E:
        bd.rollback()
        codigo = 500
        message = f"ERROR:{E}"

    response = {
        "statusCode": codigo,
        'headers': {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
        },
        "body": json.dumps({
            "statusCode": codigo,
            "message": message
        })
    }
    return response

def retManageInfo(event, context):
    try:
        data = json.loads(event['body'])
        bd = Session()
        consult1 = bd.query(Property,
                            City.name.label('propcity'),
                            State.name.label('propstate'))\
            .join(City, Property.city == City.id)\
            .join(State, Property.state == State.id)\
            .filter(Property.id == data['id'])

        consult2 = bd.query(PropertyPrice)\
            .filter((PropertyPrice.property == data['id'])
                    & (PropertyPrice.status == 1))

        for row in consult2:
            value = row.value
        for row in consult1:
            if row.Property.drei == None:
                dic_data = {
                    "number": row.Property.id,
                    "curPrice": value,
                    "drei": "None",
                    "status": row.Property.status,
                    "address": row.Property.address,
                    "state": row.propstate,
                    "city": row.propcity,
                    "zipCode": row.Property.zipcode,
                    "purchDate": row.Property.purch_date,
                    "purchDate": str(row.Property.purch_date.strftime("%Y-%m-%d")),
                    "purchPrice": row.Property.purch_price
                }
            else:
                dic_data = {
                    "number": row.Property.id,
                    "curPrice": value,
                    "drei": row.Property.drei,
                    "status": row.Property.status,
                    "address": row.Property.address,
                    "state": row.propstate,
                    "city": row.propcity,
                    "zipcode": row.Property.zipcode,
                    "purchDate": str(row.Property.purch_date.strftime("%Y-%m-%d")),
                    "purchPrice": row.Property.purch_price
                }

        bd.close()
        codigo = 200

        message = dic_data
    except IntegrityError as e:

        codigo = 500
        message = f"ERROR:{e}"
    except Exception as e:

        codigo = 500
        message = f"ERROR:{e}"

    response = {
        "statusCode": codigo,
        'headers': {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
        },
        "body": json.dumps({
            "statusCode": codigo,
            "message": message
        })
    }
    return response
