import json
from PyPDF2 import PdfFileWriter, PdfFileReader
import io
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter

class AcreditCert():
    def __init__(self):
        self.__data = None

    def fillAcreditCert(self, dataInput):  # rellena el Accredited Investor Certification
        self.__data = dataInput
        try:

            self.__packet = io.BytesIO()
            # establece formato de la hoja
            self.__can = canvas.Canvas(self.__packet, pagesize=letter)
            # page 1 and 2
            self.__can.showPage()
            self.__can.showPage()
            self.__can.setFont('Helvetica', 9)
            # page 3
            self.__can.drawString(206, 223, self.__data.get('Initials', ''))         ###azul: confirmation initials
            self.__can.drawString(102, 179, self.__data.get('day', ''))              #day of filling
            self.__can.drawString(154, 179, self.__data.get('month', ''))            #month of filling
            self.__can.setFont('Helvetica', 10)
            self.__can.drawString(219, 179, self.__data.get('year', ''))             #year of filling
            self.__can.setFont('Helvetica', 9)
            self.__can.drawString(254, 136, self.__data.get('By', ''))               ###azul by 
            self.__can.drawString(265, 114, self.__data.get('Name', ''))             ###azul Name
            self.__can.showPage()

            self.__can.save()
            self.__packet.seek(0)
            self.__NewPdf = PdfFileReader(self.__packet)
            self.__ExistingPdf = PdfFileReader(open("AcreditCert.pdf", "rb"))
            self.__output = PdfFileWriter()
            self.__page = []
            # mezcla la plantilla transparente con el pdf existente
            for i in range(3):
                self.__page.append(self.__ExistingPdf.getPage(i))
                self.__page[i].mergePage(self.__NewPdf.getPage(i))
                self.__output.addPage(self.__page[i])
            # genera el pdf de salida
            self.__outputStream = open('AcreditCertMod.pdf', "wb")
            # guarda la fusion de los archivos en el pdf de salida
            self.__output.write(self.__outputStream)
            # cierra el pdf creado
            self.__outputStream.close()
            # operacion exitosa
            self.__res = '1'
        except Exception as e:
            # devuelve la excepcion si sale mal
            self.__res = str(e)
            print(str(e))
        return self.__res
    
    
