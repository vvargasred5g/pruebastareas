import json
from AcreditCert import AcreditCert
from SubsAgreement import SubsAgreement


def hello(event, context):
    selector = 1
    data = json.loads(event['body'])
    doc1 = AcreditCert()
    doc2 = SubsAgreement()

    if (selector == 1):
        return doc1.fillAcreditCert(data)
    else:
        return doc2.fillSubsAgreement(data)
