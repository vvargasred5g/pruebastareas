import json


def sumador(event, context):
    data = json.loads(event['body'])
    resultado = 0

    for datos in data:
        resultado += int(data[datos])

    resultado = f'{resultado}'
    response = {
        "statusCode": 200,
        "headers": {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'OPTIONS, POST, GET',
            'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
        },
        "body": json.dumps({
            "success": "true|false",
            "data": {
                'resultado': resultado,
            },
            "message": "string|array"
        })
    }

    return response
