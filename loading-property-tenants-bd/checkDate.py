def checkDate(Date):
    if len(Date) != 10:
        print("ERROR: format isn't correct")
        return False
    else:
        if Date[4] != '-' or Date[7] != '-':
            print("ERROR: date separators can´t be different from '-' ")
            return False
        else:
            if int(Date[5]+Date[6])>12:
                print("ERROR: there is not months upper to 12")
                return False
            elif int(Date[5]+Date[6])<0:
                print("ERROR: there is not negative months")
                return False
            else:
                if int(Date[8]+Date[9])>31:
                    day=int(Date[8]+Date[9])
                    print(f"ERROR: not exist {day}th day of any month")
                    return False
                elif int(Date[8]+Date[9])<0:
                    print("ERROR: there is not negative days")
                    return False
                else:
                    return True