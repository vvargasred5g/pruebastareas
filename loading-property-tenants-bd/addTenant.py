import json
from db import Session
from sqlalchemy.exc import IntegrityError
from property_tenant import PropertyTenant
from checkDate import checkDate

def addTenantFunction(event, context):
    data=json.loads(event['body'])
    bd = Session()
    try:
        valid = True
        for p in data:
            if not str(data[p]).strip() or str(data[p]).strip() == None:
                valid = False
            
        if valid:
            valid2=checkDate(list(data['entry_date']))
            valid3=checkDate(list(data['contract_date']))
            valid4=checkDate(list(data['contract_end']))
            if valid2 and valid3 and valid4:
                tenant = PropertyTenant(
                    property = data['property'],
                    name = data['name'],
                    phone = data['phone'],
                    address = data['address'],
                    mail = data['mail'],
                    entry_date = data['entry_date'],
                    contract_date = data['contract_date'],
                    contract_end = data['contract_end']
                    )
                bd.add(tenant)
                bd.commit()
                codigo = 200
                message = "tenant register sucessfully"
            else:
                codigo = 500
                message = "wrong format for datetime"
                bd.rollback()   
        else:
            codigo = 500
            message = f"ERROR: a field isn´t filled"
            bd.rollback()
            
    except IntegrityError as e:
        message = str(e)
        codigo = 500
        bd.rollback()
        print(str(e))
    except Exception as e:
        codigo = 500
        message = f"ERROR: {e}"
        bd.rollback()
    bd.close()
    response = {
        "statusCode": codigo,
        'headers': {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
        },
        "body": json.dumps({
            "statusCode": codigo,
            "message": message
        })
    }
    return response


    