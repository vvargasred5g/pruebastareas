import json
from PyPDF2 import PdfFileWriter, PdfFileReader
import io
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter
import boto3
from botocore.exceptions import ClientError

class SubsAgreement():
    def __init__(self):
        self.__data = None
        self.__clientS3 = boto3.client('s3')

    def fillSubsAgreement(self, dataInput):
        self.__data = dataInput
        try:
            self.__packet = io.BytesIO()
            self.__can = canvas.Canvas(self.__packet, pagesize=letter)
            #page 1 -->0
            self.__can.drawString(170, 694, self.__data.get('NameSubscriber', ''))      #(amarillo) Name of subscriber
            self.__can.showPage()
            #page 2 -->1
            self.__can.drawString(178, 607, self.__data.get('SubTotCap', ''))           #(amarillo) aggregate subscription totaling
            self.__can.drawString(369, 595, self.__data.get('PriorDay', ''))            #(azul) prior to october [day] 2021
            self.__can.showPage()
            #page 3-5
            #for i in range(2,5,1):
            #    self.__can.showPage()
            #page 6 -->2
            self.__can.drawString(115, 145, self.__data.get('Op1', ''))                 #(azul) natural person whose individual net worth
            self.__can.showPage()
            #page 7 -->3
            self.__can.drawString(115, 672, self.__data.get('Op2', ''))                 #(azul) natural person who had an individual income
            self.__can.drawString(115, 622, self.__data.get('Op3', ''))                 #(azul) organization described in section 501(c)(3)
            self.__can.drawString(115, 560, self.__data.get('Op4', ''))                 #(azul) Any trust
            self.__can.drawString(115, 510, self.__data.get('Op5', ''))                 #(azul) Any entity
            self.__can.drawString(115, 484, self.__data.get('Op6', ''))                 #(azul) other
            self.__can.showPage()
            #page 8-10
            #for i in range(7,10,1):
            #    self.__can.showPage()
            #page 11 -->4
            self.__can.drawString(73, 607, self.__data.get('PrintName', ''))            #(amarillo) print name
            self.__can.drawString(73, 569, self.__data.get('SignTit', ''))              #(amarillo) signature/title
            self.__can.drawString(73, 531, self.__data.get('NumStreetAddr', ''))        #(amarillo) number and street address
            self.__can.drawString(73, 493, self.__data.get('city', ''))                 #(amarillo) city, state and zip code
            self.__can.drawString(73, 455, self.__data.get('EIN', ''))                  #(amarillo) EIN/social security number
            self.__can.drawString(125, 417, self.__data.get('Telephone', ''))           #(amarillo) telephone
            self.__can.drawString(96, 390.4, self.__data.get('Fax', ''))                #(amarillo) fax
            self.__can.drawString(111, 366.8, self.__data.get('Email', ''))             #(amarillo) email
            self.__can.drawString(458, 607, self.__data.get('SubsAmount', ''))          #(amarillo) total subscription amount
            
            self.__can.drawString(73, 328.5, self.__data.get('IndOwn', ''))             #(azul) individual ownership
            self.__can.drawString(73, 316, self.__data.get('TennCommon', ''))           #(azul) tennants in common
            self.__can.drawString(73, 303.5, self.__data.get('TennSurvivorship', ''))   #(azul) joint tennants with right of survivorship
            self.__can.drawString(73, 291, self.__data.get('ComProperty', ''))          #(azul) community property
            self.__can.drawString(73, 278.5, self.__data.get('PartnerCompany', ''))     #(azul) partership or limited liability company
            self.__can.drawString(73, 266, self.__data.get('Entity', ''))               #(azul) entity
            self.__can.drawString(73, 253.5, self.__data.get('Trust', ''))              #(azul) trust
            self.__can.drawString(73, 241, self.__data.get('Corporation', ''))          #(azul) corporation
            self.__can.drawString(73, 228.5, self.__data.get('OtherCheckbox', ''))      #(azul) checkbox of "other" field
            self.__can.drawString(248, 228.5, self.__data.get('OtherSpecify', ''))      #(azul) please indicate the other manner 
            self.__can.showPage()

            #page 12 -->5
            self.__can.drawString(296, 531, self.__data.get('By', ''))                  #(amarillo) by: client
            self.__can.drawString(354, 468, self.__data.get('Date', ''))                #(amarillo) dated: date
            self.__can.showPage()
            
            #page 13-24
            #for i in range(12,24,1):
            #    self.__can.showPage()
            
            self.__can.save()
            self.__packet.seek(0)
            self.__NewPdf = PdfFileReader(self.__packet)
            self.__obj = self.__clientS3.get_object(
                Bucket='serverlesbucket-15973', Key='SubsAgreement.pdf')
            self.__info = self.__obj['Body'].read()
            self.__ExistingPdf = PdfFileReader(io.BytesIO(self.__info))
            self.__output = PdfFileWriter()
            self.__page = []
            #mezcla la plantilla transparente con el pdf existente
            #for i in range(24):
            self.__page[0].append(self.__ExistingPdf.getPage(0))
            self.__page[1].append(self.__ExistingPdf.getPage(1))
            self.__page[2].append(self.__ExistingPdf.getPage(5))
            self.__page[3].append(self.__ExistingPdf.getPage(6))
            self.__page[4].append(self.__ExistingPdf.getPage(10))
            self.__page[5].append(self.__ExistingPdf.getPage(11))
            
            for i in range(6):
                self.__page[i].mergePage(self.__NewPdf.getPage(i))
                
            self.__output.addPage(self.__page[0])
            # genera el pdf de salida
            self.__outputStream = open('/tmp/SubsAgreementmod.pdf', "wb")
            # guarda la fusion de los archivos en el pdf de salida
            self.__output.write(self.__outputStream)
            # cierra el pdf creado
            self.__outputStream.close()
            self.__response = self.__clientS3.upload_file(
                '/tmp/SubsAgreementmod.pdf', 'serverlesbucket-15973', 'SubsAgreementmodified.pdf')
            # operacion exitosa
            self.__res = 'SubsAgreement document updated successfully'
        except Exception as e:
            # devuelve la excepcion si sale mal
            self.__res = str(e)
            print(str(e))
        return self.__res

