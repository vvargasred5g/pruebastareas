import json
from SubsAgreement import SubsAgreement


def FunctionFillSubsAgreement(event, context):
    data = json.loads(event['body'])
    doc2 = SubsAgreement()
    return doc2.fillSubsAgreement(data)
