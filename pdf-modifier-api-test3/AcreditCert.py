import json
from PyPDF2 import PdfFileWriter, PdfFileReader
import io
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter
import boto3
from botocore.exceptions import ClientError



class AcreditCert():
    def __init__(self):
        self.__data = None
        self.__clientS3 = boto3.client('s3')

    # rellena el Accredited Investor Certification
    def fillAcreditCert(self, dataInput):
        self.__data = dataInput
        try:
            self.__packet = io.BytesIO()
            # establece formato de la hoja
            self.__can = canvas.Canvas(self.__packet, pagesize=letter)
            # page 1 
            self.__can.setFont('Helvetica', 9)
            self.__can.drawString(57, 512,'x')  # checkbox acredited individual
            self.__can.drawString(140, 491.5,'x')  # (1) questionnaire for individual
            self.__can.drawString(140, 433.3,'x')  # (2) questionnaire for individual
            self.__can.drawString(140, 287.3,'x')  # (3) questionnaire for individual
            self.__can.drawString(140, 256.6,'x')  # (4) questionnaire for individual
            self.__can.drawString(140, 212.2,'x')  # (5) questionnaire for individual
            self.__can.drawString(93.8, 144.4,'x')  # not acredited investor checkbox
            self.__can.drawString(61.2, 91.68,'x')  # not acredited investor sign
            
            self.__can.showPage()
            
            #page 2
            self.__can.setFont('Helvetica', 9)
            self.__can.drawString(57, 641.2,'x')  # checkbox acredited partnership
            self.__can.drawString(140, 622.6,'x')  # (1) questionary for corporations
            self.__can.drawString(140, 591.7,'x')  # (2) questionary for corporations
            self.__can.drawString(140, 546.8,'x')  # (3) questionary for corporations
            self.__can.drawString(140, 515.4,'x')  # (4) questionary for corporations
            self.__can.drawString(140, 484.4,'x')  # (5) questionary for corporations
            self.__can.drawString(140, 441.4,'x')  # (6) questionary for corporations
            self.__can.drawString(140, 421.7,'x')  # (7) questionary for corporations
            self.__can.drawString(140, 378.4,'x')  # (8) questionary for corporations
            self.__can.drawString(140, 334.7,'x')  # (9) questionary for corporations
            self.__can.drawString(140, 301.9,'x')  # (10) questionary for corporations
            self.__can.drawString(140, 258.1,'x')  # (11) questionary for corporations
            self.__can.drawString(201.2, 214.4,'x')  # (11-a)
            self.__can.drawString(201.2, 170.6,'x')  # (11-b)
            self.__can.drawString(201.2, 151.9,'x')  # (11-c)
            self.__can.drawString(140, 120.3,'x')  # (12) questionary for corporations
            
            self.__can.showPage()
            
            # page 3
            self.__can.setFont('Helvetica', 9)
            self.__can.drawString(140, 671.7, 'x')  # (13) questionary for corporations
            self.__can.drawString(201.2, 626.9, 'x')  # (13-a) checkbox acredited individual
            self.__can.drawString(201.2, 596.3, 'x')  # (13-b) checkbox acredited individual
            self.__can.drawString(201.2, 577.6, 'x')  # (13-c) checkbox acredited individual
            self.__can.drawString(201.2, 558.9, 'x')  # (13-d) checkbox acredited individual
            self.__can.drawString(201.2, 539.4, 'x')  # (13-e) checkbox acredited individual
            self.__can.drawString(140, 521.6, 'x')  # (14) questionary for corporations
            self.__can.drawString(140, 452.8, 'x')  # (15) questionary for corporations
            self.__can.drawString(140, 407.75, 'x')  # (16) questionary for corporations
            self.__can.drawString(140, 314, 'x')  # (17) questionary for corporations
            self.__can.drawString(206, 223, self.__data.get('Initials', ''))  # (azul) confirmation initials
            self.__can.setFont('Helvetica', 9)
            self.__can.drawString(102, 179, 
                                  self.__data.get('day', ''))  # day of filling
            self.__can.drawString(154, 179, 
                                  self.__data.get('month', ''))  # month of filling
            
            self.__can.setFont('Helvetica', 10)
            
            self.__can.drawString(219, 179, 
                                  self.__data.get('year', ''))  # year of filling
            self.__can.setFont('Helvetica', 9)
            self.__can.drawString(254, 136, 
                                  self.__data.get('By', ''))  # (azul) by
            self.__can.drawString(265, 114, 
                                  self.__data.get('Name', ''))  # (azul) Name
            self.__can.showPage()

            self.__can.save()
            self.__packet.seek(0)
            self.__NewPdf = PdfFileReader(self.__packet)
            #self.__obj = self.__clientS3.get_object(
            #    Bucket='serverlesbucket-15973', Key='AcreditCert.pdf')
            #self.__info = self.__obj['Body'].read()
            #self.__ExistingPdf = PdfFileReader(io.BytesIO(self.__info))
            self.__ExistingPdf = PdfFileReader('AcreditCert.pdf')
            self.__output = PdfFileWriter()
            self.__page = []
            #mezcla la plantilla transparente con el pdf existente
            for i in range(3):
                self.__page.append(self.__ExistingPdf.getPage(i))
                self.__page[i].mergePage(self.__NewPdf.getPage(i))
                self.__output.addPage(self.__page[i])
            # genera el pdf de salida
            #self.__outputStream = open('/tmp/AcreditCertMod.pdf', "wb")
            self.__outputStream = open('tmp/AcreditCertMod.pdf', "wb")
            # guarda la fusion de los archivos en el pdf de salida
            self.__output.write(self.__outputStream)
            # cierra el pdf creado
            self.__outputStream.close()
            #self.__response = self.__clientS3.upload_file(
            #    '/tmp/AcreditCertMod.pdf', 'serverlesbucket-15973', 'AcreditCertModified.pdf')
            # operacion exitosa
            self.__res = 'AcreditCert document updated succesfully'
        except ClientError as e:
            # devuelve la excepcion si sale mal
            self.__res = str(e)
            print(str(e))
        return self.__res

